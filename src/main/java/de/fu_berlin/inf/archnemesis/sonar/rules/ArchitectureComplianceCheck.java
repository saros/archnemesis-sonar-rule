/*
 * Copyright (C) 2009-2014 SonarSource SA
 * All rights reserved
 * mailto:contact AT sonarsource DOT com
 */
package de.fu_berlin.inf.archnemesis.sonar.rules;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.sonar.api.rule.RuleKey;
import org.sonar.check.Priority;
import org.sonar.check.Rule;
import org.sonar.plugins.java.api.JavaFileScanner;
import org.sonar.plugins.java.api.JavaFileScannerContext;
import org.sonar.plugins.java.api.tree.AnnotationTree;
import org.sonar.plugins.java.api.tree.BaseTreeVisitor;
import org.sonar.plugins.java.api.tree.ClassTree;
import org.sonar.plugins.java.api.tree.CompilationUnitTree;
import org.sonar.plugins.java.api.tree.ExpressionTree;
import org.sonar.plugins.java.api.tree.IdentifierTree;
import org.sonar.plugins.java.api.tree.ImportTree;
import org.sonar.plugins.java.api.tree.MethodInvocationTree;
import org.sonar.plugins.java.api.tree.MethodTree;
import org.sonar.plugins.java.api.tree.Tree;

import de.fu_berlin.inf.archnemesis.archnemesis.Architecture;
import de.fu_berlin.inf.archnemesis.archnemesis.Component;
import de.fu_berlin.inf.archnemesis.archnemesis.Constraint;
import de.fu_berlin.inf.archnemesis.archnemesis.RootComponent;
import de.fu_berlin.inf.archnemesis.sonar.JavaExtensionRulesRepository;
import de.fu_berlin.inf.archnemesis.sonar.chralx.ChralxSingleton;
import de.fu_berlin.inf.archnemesis.sonar.rules.util.TreeUtils;

/**
 * This class is an example of how to implement your own rules. The (stupid)
 * rule raises a minor issue each time a method is encountered. The @Rule
 * annotation allows to specify the rule key, name, description and default
 * severity.
 */
@Rule(key = ArchitectureComplianceCheck.KEY, priority = Priority.MAJOR, name = "Chralx violation", description = "Architecture compliance checking based on SAROS_ROOT/architecture.chralx")
/**
 * The class extends BaseTreeVisitor: the visitor for the Java AST.
 * The logic of the rule is implemented by overriding its methods.
 * It also implements the JavaFileScanner interface to be injected with the JavaFileScannerContext to attach issues to this context.
 */
public class ArchitectureComplianceCheck extends BaseTreeVisitor implements
		JavaFileScanner {

	public static final String KEY = "nomethod";
	private final RuleKey RULE_KEY = RuleKey.of(
			JavaExtensionRulesRepository.REPOSITORY_KEY, KEY);

	private static final String DEFAULT_CHRALX_LOCATION = "architecture.chralx";

	private Architecture arc;
	private Map<Component, Set<Component>> componentToAccess;
	private List<Constraint> constraints;

	/**
	 * Private field to store the context: this is the object used to create
	 * issues.
	 */
	private JavaFileScannerContext context;

	/**
	 * Implementation of the method of the JavaFileScanner interface.
	 * 
	 * @param context
	 *            Object used to attach issues to source file.
	 */
	@Override
	public void scanFile(JavaFileScannerContext context) {
		this.context = context;

		scan(context.getTree());
	}

	@Override
	public void visitCompilationUnit(CompilationUnitTree compilationUnit) {

		arc = ChralxSingleton.getInstance().getArc();
		componentToAccess = ChralxSingleton.getInstance()
				.getComponentToAccess();
		constraints = ChralxSingleton.getInstance().getConstraints();

		// AST Analysis
		List<ImportTree> imports = compilationUnit.imports();
		Set<Component> components = componentToAccess.keySet();

		// check constraints
		for (Constraint c : constraints)
		{
			applyConstraintCheck(compilationUnit, imports, c);
		}

		// check component access
		for (Component component : components) {
			
			if (isInComponent(compilationUnit, component)) {
				applyLayerAccessCheck(imports, components, component);
			}
		}

		super.visitCompilationUnit(compilationUnit);
	}

	private void applyConstraintCheck(CompilationUnitTree compilationUnit,
			List<ImportTree> imports, Constraint c) {
		for (ImportTree i : imports) {
		
			if (isInComponent(i, c.getComponent())) {
			
				if (!isInImports(c.getRequiredPackage(), imports)){					
					context.addIssue(compilationUnit, RULE_KEY, "The component "
							+ c.getComponent().getName()
							+ " should generally be used together with "
							+ c.getRequiredPackage()
							+ ". Are you sure this code is not faulty?");
					break;
				}
			}
		}
	}

	private boolean isInImports(String requiredPackage, List<ImportTree> imports) {
		// TODO Auto-generated method stub
		return isInNamespaces(requiredPackage, toStringList(imports));
	}

	private void applyLayerAccessCheck(List<ImportTree> imports,
		Set<Component> components, Component component) {
		Set<Component> accessibleComponents = componentToAccess.get(component);

		for (int i = 0; i < imports.size(); i++) {
			ImportTree importt = imports.get(i);

			if (isInComponents(importt, components)) {
				if (!isInComponents(importt, accessibleComponents)) {
					context.addIssue(importt, RULE_KEY,
							"Please refer to the architecture DSL arhitecture.chralx under SAROS_ROOT.");

				}
			}
		}
	}

	private List<String> toStringList(List<ImportTree> imports) {
		List<String> list = new ArrayList<String>();
		for (ImportTree i : imports) {
			list.add(TreeUtils.concatenate((ExpressionTree) i
					.qualifiedIdentifier()));
		}
		return list;
	}

	private boolean isInComponents(ImportTree imp, Set<Component> set) {
		boolean result = false;
		for (Component c : set) {
			result |= isInComponent(imp, c);
		}
		return result;
	}

	private boolean isInComponent(ImportTree package_, Component component) {
		String qName = TreeUtils.concatenate((ExpressionTree) package_
				.qualifiedIdentifier());
		return isInNamespaces(qName, component.getNamespaces());
	}

	private boolean isInComponent(CompilationUnitTree package_,
			Component component) {
		String qName = TreeUtils.concatenate(package_.packageName());
		return isInNamespaces(qName, component.getNamespaces());
	}

	private boolean isInNamespace(String qName, String namespace) {
		return qName.matches(qualifiedNameToRegex(namespace));
	}

	private boolean isInNamespaces(String qName, List<String> namespaces) {
		boolean isIn = false;
		for (String namespace : namespaces) {
			isIn |= isInNamespace(qName, namespace);
		}

		return isIn;
	}

	private String qualifiedNameToRegex(String qualifiedName) {
		qualifiedName = qualifiedName.replaceAll("\\.", "\\\\.");
		return qualifiedName = qualifiedName + ".*";
	}

}
