package de.fu_berlin.inf.archnemesis.sonar.chralx;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Inject;
import com.google.inject.Injector;

import de.fu_berlin.inf.archnemesis.ArchnemesisStandaloneSetup;
import de.fu_berlin.inf.archnemesis.archnemesis.Architecture;

public class XtextParser {
	@Inject
	private XtextResourceSet resourceSet;

	public XtextParser() {
		setupParser();
	}

	private void setupParser() {
		Injector injector = new ArchnemesisStandaloneSetup()
				.createInjectorAndDoEMFRegistration();
		injector.injectMembers(this);
		
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL,
				Boolean.TRUE);
	}

	/**
	 * Parses data provided by an input reader using Xtext and returns the root
	 * node of the resulting object tree.
	 * 
	 * @param reader
	 *            Input reader
	 * @return root object node
	 * @throws IOException
	 *             when errors occur during the parsing process
	 */

	public EObject parse(String absoluteChralxPath) {
		Resource resource = resourceSet.getResource(
				URI.createURI("file:" + absoluteChralxPath), true);
		return (Architecture) resource.getContents().get(0);
	}
}
